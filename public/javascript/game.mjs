import {
  buildRoomCard,
  buildRoom,
  buildGameField,
  buildCommentator,
} from "./builders/builders.mjs";

const roomsPage = document.getElementById("rooms-page");
const gamePage = document.getElementById("game-page");

let gameField;

const createRoomBtn = document.getElementById("add-room-btn");

const roomCardsContainer = document.getElementById("room-cards-container");

const username = sessionStorage.getItem("username");

let currentRoom = null;
let isReady = false;
let gameText;

if (!username) {
  window.location.replace("/login");
}

const socket = io("", { query: { username } });

const createRoomHandler = () => {
  const roomName = prompt("Enter room name:");
  if (roomName === null) return;
  socket.emit("CREATE_ROOM", roomName);

  socket.on("CREATE_ROOM_DONE", (room) => {
    currentRoom = room.name;
    renderRoom(room);
  });
};

createRoomBtn.addEventListener("click", createRoomHandler);

const joinRoomHandler = (event) => {
  if (event.target.tagName === "BUTTON") {
    const roomName = event.target.dataset.id;
    socket.emit("JOIN_ROOM", roomName);
  }
  return;
};

const leaveRoomHandler = () => {
  roomsPage.classList.remove("display-none");
  gamePage.classList.add("display-none");
  socket.emit("LEAVE_ROOM", currentRoom);
  currentRoom = null;
  isReady = false;
};

const nameTakenHandler = (username) => {
  sessionStorage.removeItem("username");
  window.location.replace("/login");
  alert(`Username ${username} is already taken!`);
};

const roomNameTakenHandler = () => {
  alert(`This room name is already taken`);
};

const updateRoomsHandler = (rooms) => {
  console.log(rooms);
  roomCardsContainer.innerHTML = "";
  for (const room of rooms) {
    if (room.display === true) {
      const roomCard = buildRoomCard(room.name, room.users.length);
      roomCard.addEventListener("click", joinRoomHandler);
      roomCardsContainer.appendChild(roomCard);
    }
  }
};

const updateRoomHandler = (room) => {
  if (currentRoom === room.name) {
    renderRoom(room);
  }
};

const countdownHandler = (countdown, room) => {
  const quitBtn = gamePage.querySelector("#quit-room-btn");
  quitBtn.style.visibility = "hidden";

  gameField.innerHTML = `<div id="timer">${countdown}</div>`;
  const countdownTimer = setInterval(() => {
    gameField.innerHTML = `<div id="timer">${--countdown}</div>`;
    if (countdown <= 0) {
      startGame(room);
      clearInterval(countdownTimer);
    }
  }, 1000);
};

const busyRoomHandler = (room) => {
  const roomCard = document.getElementById(room.name);
  roomCard.classList.add("display-none");
};

socket.on("USERNAME_ALREADY_TAKEN", nameTakenHandler);
socket.on("ROOMNAME_ALREADY_TAKEN", roomNameTakenHandler);
socket.on("UPDATE_ROOMS", updateRoomsHandler);
socket.on("UPDATE_ROOM", updateRoomHandler);
socket.on("JOIN_ROOM_DONE", (room) => {
  console.log(`Joined to ${room.name}`);
  currentRoom = room.name;
  renderRoom(room);
  return;
});
socket.on("JOIN_ROOM_FAIL", (roomName) => {
  alert(`Unable to join ${roomName}`);
});
socket.on("COUNTDOWN_STARTED", countdownHandler);
socket.on("ROOM_IS_BUSY", busyRoomHandler);

const renderRoom = (room) => {
  roomsPage.classList.add("display-none");
  gamePage.classList.remove("display-none");

  const roomEl = buildRoom(room.name, room.users);
  gamePage.innerHTML = roomEl.innerHTML;

  const quitBtn = gamePage.querySelector("#quit-room-btn");
  quitBtn.style.visibility = "visible";
  quitBtn.addEventListener("click", leaveRoomHandler);

  const renderUser = (ready) => {
    const userEl = gamePage.querySelector(`.user.${username}`);
    const readyStatusEl = userEl.querySelector("span");
    readyStatusEl.classList.add(`ready-status-${ready ? "green" : "red"}`);
    readyStatusEl.classList.remove(`ready-status-${ready ? "red" : "green"}`);
  };

  const renderGameField = () => {
    gameField = gamePage.querySelector(".game-container");
    const gameFieldEl = buildGameField(isReady);
    gameField.innerHTML = gameFieldEl.innerHTML;

    const readyBtn = gamePage.querySelector("#ready-btn");
    readyBtn.addEventListener("click", () => {
      isReady = !isReady;
      socket.emit("USER_READY", isReady);
      renderUser(isReady);
      renderGameField();
    });
  };

  renderGameField();
};

const startGame = (room) => {
  const renderCommentator = () => {
    const commentatorEl = document.querySelector(".commentator");
    const commentator = buildCommentator("Test phrase");
    commentatorEl.innerHTML = "";
    commentatorEl.appendChild(commentator);
  };

  let gameTime = 60;
  const gameTimer = setInterval(() => {
    const timerEl = document.querySelector(".timer-game");
    gameTime--;
    timerEl.textContent = `${gameTime} seconds left`;

    renderCommentator();
    if (gameTime <= 0) {
      socket.emit("GAME_ENDED_TIME", room);
      clearInterval(gameTimer);
    }
  }, 1000);

  gameText = room.text.split("");
  gameField.innerHTML = `<div class="timer-game">${gameTime} seconds left</div>
                        <div class="text-container">${room.text}</div>`;

  let currentPos = 0;
  let progress = 0;

  const keyUpHandler = (e) => {
    if (e.key === gameText[currentPos]) {
      currentPos++;
      progress = Math.floor((currentPos / gameText.length) * 100);
      socket.emit("USER_PROGRESS", progress);

      const textContainer = document.querySelector(".text-container");
      const writtenText = gameText.slice(0, currentPos).join("");
      const currentSymbol = gameText.slice(currentPos, currentPos + 1);
      const otherText = gameText
        .slice(currentPos + 1, gameText.length)
        .join("");
      textContainer.innerHTML = `<span style="background: rgba(0, 172, 0, 0.7);">${writtenText}</span>
                                <span class='current'">${
                                  currentSymbol === " "
                                    ? "&nbsp;"
                                    : currentSymbol
                                }</span>
                                ${otherText}`;
    }
  };

  window.addEventListener("keyup", keyUpHandler);
};

const progressBarHandler = (user, progress) => {
  const progressBar = document.querySelector(`.user-progress-fill.${user}`);
  progressBar.style.width = `${progress}%`;
};

const gameEndHandler = (winners, room) => {
  if (currentRoom !== room.name) return;
  const modal = document.querySelector(".modal");
  const modalContent = document.querySelector(".modal-content");

  modal.style.display = "block";
  modalContent.innerHTML = '<span class="close" id="quit-results-btn">X</span>';

  if (currentRoom === room.name) {
    for (const user of winners) {
      const place = winners.indexOf(user) + 1;
      const userStr = `<p class="place-${place}">${place}. ${user}</p>`;
      modalContent.innerHTML += userStr;
    }
  }

  window.onclick = function (event) {
    if (event.target == modal || event.target.id == "quit-results-btn") {
      modal.style.display = "none";
      isReady = false;
      socket.emit("GAME_END", room);
    }
  };
};

socket.on("UPDATE_PROGRESSBAR", progressBarHandler);
socket.on("GAME_ENDED", gameEndHandler);
