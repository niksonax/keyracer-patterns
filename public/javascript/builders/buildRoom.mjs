import { buildUser } from "./buildUser.mjs";

export const buildRoom = (name, users) => {
  const room = document.createElement("div");
  const userList = document.createElement("div");
  userList.classList.add("user-list");
  for (const user of users) {
    const userEl = buildUser(user.name, user.isReady, 0);
    userList.appendChild(userEl);
  }
  room.innerHTML = `
            <div class="info-container">
                <h2 class="room-name">${name}</h2>
                <button id="quit-room-btn">Back to rooms</button>
                ${userList.innerHTML}
            </div>
            <div class="game-container">
                <p class="game-text">
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ducimus
                    vero accusantium neque nesciunt blanditiis atque, pariatur, id odit
                    provident, officiis similique. Esse quam aperiam praesentium, quidem
                    ab suscipit perferendis id!
                </p>
            </div>
            <div class="commentator"></div> `;

  return room;
};
