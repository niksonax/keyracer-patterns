export const buildCommentator = (phrase) => {
  const commentator = document.createElement("div");
  commentator.id = "commentator";
  commentator.innerHTML = `<i class="fas fa-user-tie 5x"></i>
                          <p>${phrase}</p>
                        `;
  return commentator;
};
