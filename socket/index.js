import * as config from "./config";
import { chooseRandomText } from "./helpers";

const usernames = new Set();
const rooms = new Set();
let winners = [];

export default (io) => {
  io.on("connection", (socket) => {
    console.log(socket.id, "connected");
    const username = socket.handshake.query.username;

    socket.emit("UPDATE_ROOMS", Array.from(rooms));

    if (usernames.has(username)) {
      socket.emit("USERNAME_ALREADY_TAKEN", username);
      return;
    }
    usernames.add(username);
    socket.emit("USER_LOGGED_IN", username);

    socket.on("CREATE_ROOM", (roomName) => {
      if (findRoom(roomName)) {
        socket.emit("ROOMNAME_ALREADY_TAKEN");
        return;
      }
      const room = {
        name: roomName,
        text: "",
        display: true,
        users: [{ name: username, isReady: false }],
      };
      rooms.add(room);

      socket.emit("CREATE_ROOM_DONE", room);
      socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms));
    });

    socket.on("JOIN_ROOM", (roomName) => {
      const room = findRoom(roomName);
      const res = joinRoom(room, username);

      if (res) {
        socket.emit("JOIN_ROOM_DONE", room);
        socket.broadcast.emit("UPDATE_ROOM", room);
        socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms));
      } else {
        socket.emit("JOIN_ROOM_FAIL", roomName);
      }
      return;
    });

    socket.on("LEAVE_ROOM", (roomName) => {
      console.log("LEAVE_ROOM");
      const room = findRoom(roomName);
      updateUserProgress(username, 0);
      leaveRoom(room, username, socket);
      socket.emit("UPDATE_ROOMS", Array.from(rooms));
      return;
    });

    socket.on("USER_READY", (isReady) => {
      const room = findRoomByUserName(username);
      setUserReady(room, username, isReady, socket);
      socket.emit("UPDATE_ROOM", room);
      socket.broadcast.emit("UPDATE_ROOM", room);
      console.log(room);
    });

    socket.on("USER_PROGRESS", (progress) => {
      const room = updateUserProgress(username, progress);

      socket.emit("UPDATE_PROGRESSBAR", username, progress);
      socket.broadcast.emit("UPDATE_PROGRESSBAR", username, progress);

      const winnersArr = checkForWinner(room);
      if (winnersArr) {
        room.display = true;
        console.log("GAME_ENDED", winners);
        socket.emit("GAME_ENDED", winners, room);
        socket.broadcast.emit("GAME_ENDED", winners, room);
        socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms));
      }
    });

    socket.on("GAME_ENDED_TIME", (room) => {
      const roomUsers = getRoomUsers(room.name);
      for (const user of roomUsers) {
        if (!winners.includes(user.name)) {
          winners.push(user.name);
        }
      }
      room.display = true;
      socket.emit("GAME_ENDED", winners, room);
      socket.broadcast.emit("GAME_ENDED", winners, room);
      socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms));
    });

    socket.on("GAME_END", (room) => {
      winners = [];
      const roomUsers = getRoomUsers(room.name);
      for (const user of roomUsers) {
        user.progress = 0;
        user.isReady = false;
      }
      console.log("END_GAME_UPDATE", room);
      socket.emit("UPDATE_ROOM", room);
      socket.broadcast.emit("UPDATE_ROOM", room);
    });

    socket.on("disconnect", () => {
      console.log(socket.id, "disconnected");
      const room = findRoomByUserName(username);
      console.log("disconnect leave", room);
      if (room) leaveRoom(room, username, socket);
      usernames.delete(username);
    });
  });
};

const findRoom = (roomName) => {
  const [room] = Array.from(rooms).filter((room) => room.name === roomName);
  return room;
};

const findRoomByUserName = (username) => {
  for (const room of Array.from(rooms)) {
    for (const user of room.users) {
      if (user.name === username) return room;
    }
  }
};

const getRoomUsers = (roomName) => {
  const room = findRoom(roomName);
  return room.users;
};

const joinRoom = (room, username) => {
  console.log(`User ${username} connecting to ${room.name}`);
  if (room.users.length < config.MAXIMUM_USERS_FOR_ONE_ROOM) {
    const user = { name: username, isReady: false };
    room.users.push(user);
    return true;
  }
  return false;
};

const setUserReady = (room, username, ready, socket) => {
  const roomUsers = getRoomUsers(room.name);
  for (const user of roomUsers) {
    if (user.name === username) {
      user.isReady = ready;
    }
  }
  if (checkRoomReady(room)) startGame(room, socket);
};

const checkRoomReady = (room) => {
  if (room.users.length === 0) return false;
  for (const user of room.users) {
    if (!user.isReady) return false;
  }
  console.log("Room is redy:", room);
  return true;
};

const startGame = (room, socket) => {
  const text = chooseRandomText(room);
  room.text = text;

  socket.emit(
    "COUNTDOWN_STARTED",
    config.SECONDS_TIMER_BEFORE_START_GAME,
    room
  );
  socket.broadcast.emit(
    "COUNTDOWN_STARTED",
    config.SECONDS_TIMER_BEFORE_START_GAME,
    room
  );

  room.display = false;
  socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms));
};

const updateUserProgress = (username, progress) => {
  const room = findRoomByUserName(username);
  for (const user of room.users) {
    if (username === user.name) {
      user.progress = progress;
    }
  }
  return room;
};

const checkForWinner = (room) => {
  const roomUsers = getRoomUsers(room.name);
  for (const user of roomUsers) {
    if (user.progress === 100 && !winners.includes(user.name)) {
      winners.push(user.name);
    }
  }
  if (roomUsers.length === winners.length) return winners;
};

const leaveRoom = (room, username, socket) => {
  deleteUserFromRoom(room, username);

  socket.broadcast.emit("UPDATE_ROOM", room);
  socket.broadcast.emit("UPDATE_ROOMS", Array.from(rooms));

  if (checkRoomReady(room)) startGame(room, socket);

  console.log(`User ${username} left ${room.name}`);
};

const deleteUserFromRoom = (room, username) => {
  const roomUsers = getRoomUsers(room.name);
  for (const user of roomUsers) {
    if (user.name === username) {
      const userIndex = roomUsers.indexOf(user);
      roomUsers.splice(userIndex, 1);
    }
  }
};
