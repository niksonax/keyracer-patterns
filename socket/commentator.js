//import { phrases } from "../data.js";

const phrases = {
  greetings: [
    "The game will start in a few seconds. Good luck to all players!",
    "Today`s game gonna be hot! I hope our players are ready",
    "Ladies and gentlemens, welcome to the Keyracers! The show will start in a moment!",
  ],
  cars: [
    "Tesla Model S",
    "Meercedes S Class",
    "BMW i8",
    "McLaren 720s",
    "Volkswagen Golf GTI",
  ],
  userRepresents: [
    "Looks like he gonna push it to the limit today!",
    "Very imporant race for him in this season.",
    "I love this guy! Actually, he`s one of my favorites.",
  ],
  positions: ["0", "1st", "2nd", "3rd", "4th", "5th"],
};

/* FACTORY */
class PhrasesFactory {
  create(type, user, position) {
    switch (type) {
      case "greet":
        return new GreetPhrase();
      case "represent-user":
        return new RepresentUserPhrase(user);
      case "race-state":
        return new RaceStatePhrase(user, position);
      case "user-finished":
        return new UserFinishPhrase(user, position);
      default:
        break;
    }
  }
}

class GreetPhrase {
  constructor() {
    this.text = this.getRandomPhrase();
  }

  getRandomPhrase() {
    const index = Math.floor(phrases.greetings.length * Math.random());
    return phrases.greetings[index];
  }

  getPhrase() {
    return this.text;
  }
}

class RepresentUserPhrase {
  constructor(user) {
    this.user = user;
    this.car = this.getRandomCar();
    this.text = `${this.user} on the ${this.car}.`;
  }

  getRandomCar() {
    const index = Math.floor(phrases.cars.length * Math.random());
    return phrases.cars[index];
  }

  getRestPhrase() {
    const index = Math.floor(phrases.userRepresents.length * Math.random());
    return phrases.userRepresents[index];
  }

  getPhrase() {
    return this.text;
  }
}

class RaceStatePhrase {
  constructor(user, position) {
    this.user = user;
    this.position = phrases.positions[position];
    this.text = `${this.user} on the ${this.position} position.`;
  }

  getPhrase() {
    return this.text;
  }
}

class UserFinishPhrase {
  constructor(user, position) {
    this.user = user;
    this.position = phrases.positions[position];
    this.text = `${this.user} crossed the finish line! Taking the ${this.position} place.`;
  }
}

const phrasesFactory = new PhrasesFactory();
const greetPhrase = phrasesFactory.create("greet");
const representUserPhrase = phrasesFactory.create("represent-user", "Jonathan");

/* PROXY */
const userPhrasesProxy = new Proxy(representUserPhrase, {
  get: (target, prop) => {
    if (prop === "fullPhrase") {
      return `${target.text} ${target.getRestPhrase()}`;
    }
  },
});

console.log(userPhrasesProxy.fullPhrase);

/* FACADE */
class Phrase {
  fullPhrase() {
    const greet = new GreetPhrase().getPhrase();
    const represent = new RepresentUserPhrase("Samuel").getPhrase();

    return `${greet} ${represent}`;
  }
}

const phrase = new Phrase();
console.log(phrase.fullPhrase());
