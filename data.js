export const texts = [
  "Text for typing #1",
  "Text for typing #2",
  "Text for typing #3",
  "Text for typing #4",
  "Text for typing #5",
  "Text for typing #6",
  "Text for typing #7",
];

export const phrases = {
  greetings: [
    "The game will start in a few seconds. Good luck to all players!",
    "Today`s game gonna be hot! I hope our players are ready",
    "Ladies and gentlemens, welcome to the Keyracers! The show will start in a moment!",
  ],
  cars: [
    "Tesla Model S",
    "Meercedes S Class",
    "BMW i8",
    "McLaren 720s",
    "Volkswagen Golf GTI",
  ],
  positions: ["0", "1st", "2nd", "3rd", "4th", "5th"],
};

export default { texts };
